@extends('layouts/layout')

@section('content')
    <form method="post" action="{{ Route('dashboard.akun-zoom.update',['akun_zoom' => $akunZoom->id]) }}">
        @csrf
        @method("patch")
        <div class="form-group">
            <label for="formGroupExampleInput">Kategori Akun</label>
            <select name="kategori_id" id="" class="form-control">
                <option disabled hidden selected>Pilih Kategori Akun</option>
                @foreach ($categories as $category)
                <option value="{{ $category->id }}" @if($category->id == $akunZoom->kategori_id) selected @endif>{{ $category->nama_kategori }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Status Akun</label>
            <select name="status_akun" id="" class="form-control">
                <option value="aktif" @if($akunZoom->status_akun == "aktif") selected @endif>Aktif</option>
                <option value="tidak aktif" @if($akunZoom->status_akun == "tidak aktif") selected @endif>Tidak Aktif</option>
            </select>
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Nama Akun</label>
            <input type="text" class="form-control" value="{{ $akunZoom->nama_akun }}" name="nama_akun" placeholder="Masukan Nama Akun Zoom">
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Email Akun</label>
            <input type="email" class="form-control" value="{{ $akunZoom->email }}" name="email" placeholder="Masukan Email Akun Zoom">
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">password Akun</label>
            <input type="password" class="form-control" value="{{ Crypt::decryptString($akunZoom->password); }}" name="password" placeholder="Masukan Password Akun Zoom">
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Kapasitas Participant Akun</label>
            <input type="number" class="form-control" value="{{ $akunZoom->kapasitas }}" name="kapasitas" placeholder="Masukan Jumlah Participant Akun Zoom">
        </div>
        <button class="btn btn-primary">Submit</button>
    </form>
@endsection
