@extends('layouts/layout')

@section('content')
    <form method="post" action="{{ Route('dashboard.akun-zoom.store') }}">
        @csrf
        <div class="form-group">
            <label for="formGroupExampleInput">Kategori Akun</label>
            <select name="kategori_id" id="" class="form-control">
                <option disabled hidden selected>Pilih Kategori Akun</option>
                @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{ $category->nama_kategori }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Nama Akun</label>
            <input type="text" class="form-control" name="nama_akun" placeholder="Masukan Nama Akun Zoom">
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Email Akun</label>
            <input type="email" class="form-control" name="email" placeholder="Masukan Email Akun Zoom">
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">password Akun</label>
            <input type="password" class="form-control" name="password" placeholder="Masukan Password Akun Zoom">
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Kapasitas Participant Akun</label>
            <input type="number" class="form-control" name="kapasitas" placeholder="Masukan Jumlah Participant Akun Zoom">
        </div>
        <button class="btn btn-primary">Simpan</button>
    </form>
@endsection
