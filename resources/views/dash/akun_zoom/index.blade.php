@extends('layouts/layout')

@section('content')
    <table id="example" class="table table-striped" style="width:100%">
        <thead>
            <tr>
                <th>Nama Akun</th>
                <th>Email Akun</th>
                <th>Jumlah Participant Akun</th>
                <th>Status Akun</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($akuns as $akun)
                <tr>
                    <td>{{ $akun->nama_akun }}</td>
                    <td>{{ $akun->email }}</td>
                    <td>{{ $akun->kapasitas }}</td>
                    <td>{{ $akun->status_akun }}</td>
                    <td class="text-center">
                        <a class="btn btn-sm btn-primary py-2"
                            href="{{ Route('dashboard.akun-zoom.edit', ['akun_zoom' => $akun->id]) }}">Edit</a>
                        <form class="py-2"
                            action="{{ Route('dashboard.akun-zoom.destroy', ['akun_zoom' => $akun->id]) }}" method="post">
                            @csrf
                            @method("delete")
                            <button class="btn btn-sm btn-danger text-light" id="deleteBtn">Hapus</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            <a class="btn btn-primary mb-3" href="{{ Route('dashboard.akun-zoom.create') }}">Tambah Akun</a>
        </tbody>
    </table>
@endsection

@section('scripts')
    <script>
        $(document).on('click', '#deleteBtn', function(e) {
            e.preventDefault(); // prevent form submit
            var id = $(this).data('id');
            Swal.fire({
                title: 'Yakin ingin menghapus data?',
                text: "Kamu tidak akan bisa mengembalikan data saat sudah dihapus !",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) {
                    $(this).closest("form").submit();
                }
            });
        })

        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>
@endsection
