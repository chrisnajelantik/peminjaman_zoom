@extends('layouts/layout')

@section('content')
    <table id="example" class="table table-striped" style="width:100%">
        <thead>
            <tr>
                <th>Nama Akun</th>
                <th>Jumlah Akun</th>
                <th>Durasi Akun Zoom</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($categories as $category)
                <tr>
                    <td>{{ $category->nama_kategori }}</td>
                    <td>{{ $category->jumlah_akun }}</td>
                    <td>{{ $category->durasi_kategori }}</td>
                    <td class="text-center">
                        <a class="btn btn-primary py-2"
                            href="{{ Route('dashboard.kategori.edit', ['kategori' => $category->id]) }}">Edit</a>
                        <form class="py-2" action="{{ Route('dashboard.kategori.destroy', ['kategori' => $category->id]) }}"method="post">
                            @csrf
                            @method("delete")
                            <button class="btn btn-danger text-light" id="deleteBtn">Hapus</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            <a class="btn btn-primary mb-3" href="{{ Route('dashboard.kategori.create') }}">Tambah Kategori</a>
        </tbody>
    </table>
@endsection

@section('scripts')
    <script>
        $(document).on('click', '#deleteBtn', function(e) {
            e.preventDefault(); // prevent form submit
            var id = $(this).data('id');
            Swal.fire({
                title: 'Yakin ingin menghapus data?',
                text: "Kamu tidak akan bisa mengembalikan data saat sudah dihapus !",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) {
                    $(this).closest("form").submit();
                }
            });
        })

        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>
@endsection
