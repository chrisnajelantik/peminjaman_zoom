@extends('layouts/layout')

@section('content')
    <form method="post" action="{{ Route('dashboard.kategori.update', ['kategori' => $kategori->id]) }}">
        @csrf
        @method("patch")
        <div class="form-group">
            <label for="formGroupExampleInput">Nama Kategori</label>
            <input type="text" class="form-control" value="{{ $kategori->nama_kategori }}" name="nama_kategori"
                placeholder="Masukan Nama Akun Zoom">
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Jumlah Akun</label>
            <input type="number" class="form-control" value="{{ $kategori->jumlah_akun }}" name="jumlah_akun"
                placeholder="Masukan Jumlah Akun Zoom">
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Durasi Akun Zoom</label>
            <input type="text" class="form-control" value="{{ $kategori->durasi_kategori }}" name="durasi_kategori"
                placeholder="Masukan Maksimal Durasi Akun">
        </div>
        <button class="btn btn-primary">Submit</button>
    </form>
@endsection
