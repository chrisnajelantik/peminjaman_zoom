@extends('layouts/layout')

@section('content')
    <form method="post" action="{{ Route('dashboard.kategori.store') }}">
        @csrf
        <div class="form-group">
            <label for="formGroupExampleInput">Nama Kategori</label>
            <input type="text" class="form-control" name="nama_kategori" placeholder="Masukan Nama Kategori Akun Zoom">
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Jumlah Akun</label>
            <input type="number" class="form-control" name="jumlah_akun" placeholder="Masukan Jumlah Akun Zoom">
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Durasi Akun Zoom</label>
            <input type="text" class="form-control" name="durasi_kategori" placeholder="Masukan Maksimal Durasi Akun">
        </div>
        <button class="btn btn-primary">Submit</button>
    </form>
@endsection
