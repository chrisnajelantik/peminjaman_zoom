@extends('layouts/layout')

@section('content')
    <table id="example" class="table table-striped" style="width:100%">
        <thead>
            <tr>
                <th>Nama Peminjam</th>
                <th>Katgori Akun</th>
                <th>Event</th>
                <th>Tanggal Peminjaman</th>
                <th>Jam Peminjaman</th>
                <th>Durasi Peminjaman</th>
                <th>Status</th>
                <th>Keterangan</th>

                <th>Action</th>

            </tr>
        </thead>
        <tbody>
            @foreach ($peminjamans as $peminjaman)
                <tr>
                    <td>{{ $peminjaman->user->name }}</td>
                    <td>{{ $peminjaman->akun->nama_akun }}</td>
                    <td>{{ $peminjaman->event }}</td>
                    <td>{{ $peminjaman->tanggal }}</td>
                    <td>{{ $peminjaman->jam }}</td>
                    <td>{{ $peminjaman->durasi }}</td>
                    <td><span class="badge @if ($peminjaman->status == 'approved') bg-success @elseif ($peminjaman->status == 'returned') bg-primary  @elseif ($peminjaman->status == 'pending') bg-secondary @else bg-danger   @endif"> {{ $peminjaman->status }}</span></td>
                    <td>{{ $peminjaman->keterangan ?? '-' }}</td>
                    <td>
                        @if (auth()->user()->hasRole('admin'))
                            <button class="updateBtn btn btn-primary text-light py-2" data-id="{{ $peminjaman->id }}"
                                data-keterangan="{{ $peminjaman->keterangan }}" data-status="{{ $peminjaman->status }}"
                                data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@mdo">Ubah
                                Status</button>
                        @endif
                        <form class=" py-2"
                            action="{{ Route('dashboard.peminjaman.destroy', ['peminjaman' => $peminjaman->id]) }}"
                            method="post">
                            @csrf
                            @method("delete")
                            <button @if ((Auth::user()->hasRole('mahasiswa') && $peminjaman->status == 'approved') || $peminjaman->status == 'returned') disabled  @endif class="btn btn-danger text-light" id="deleteBtn">Hapus</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            @if (Auth::user()->hasRole('mahasiswa'))
                <a class="btn btn-primary mb-3" href="{{ Route('dashboard.peminjaman.create') }}">Ajukan Peminjaman</a>
            @endif
        </tbody>
    </table>

    @if (auth()->user()->hasRole('admin'))
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <form id="formUpdate" method="post">
                    @csrf
                    @method("patch")
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Update Status</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="mb-3">
                                <label for="recipient-name" class="col-form-label">Status</label>
                                <select name="status" id="selectStatus" class="form-control">
                                    <option disabled hidden>pending</option>
                                    <option value="approved">Approved</option>
                                    <option value="declined">Declined</option>
                                    <option value="returned">Returned</option>

                                </select>
                            </div>
                            <div class="mb-3 keterangan">
                                <label for="keterangan" class="col-form-label">Keterangan:</label>
                                <textarea class="form-control" name="keterangan" id="keterangan"
                                    placeholder="Masukkan Keterangan"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batalkan</button>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    @endif
@endsection

@section('scripts')
    <script>
        $(document).on('click', '#deleteBtn', function(e) {
            e.preventDefault(); // prevent form submit
            var id = $(this).data('id');
            Swal.fire({
                title: 'Yakin ingin membatalkan peminjaman?',
                text: "Kamu tidak akan bisa mengembalikan data saat sudah dihapus !",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) {
                    $(this).closest("form").submit();
                }
            });
        })

        $(document).ready(function() {
            @if (auth()->user()->hasRole('admin'))
                $("#selectStatus").on("change", function(){
                if ($(this).val() == "approved") {
                $(".keterangan").addClass("d-none");
                }else{
                $(".keterangan").removeClass("d-none");
                }
                })
                $(".updateBtn").on('click', function() {
                id = $(this).data("id");
                status = $(this).data("status");
                keterangan = $(this).data("keterangan");
            
                if (status == "approved") {
                $(".keterangan").addClass("d-none");
                }else{
                $(".keterangan").removeClass("d-none");
                }
                $('#formUpdate').attr('action', '/dashboard/peminjaman/' + id);
                $('#keterangan').val(keterangan);
                $('#selectStatus').val(status);
                })
            @endif
            $('#example').DataTable();
        });
    </script>
@endsection
