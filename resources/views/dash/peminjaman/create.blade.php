@extends('layouts/layout')

@section('content')
    <form method="post" action="{{ Route('dashboard.peminjaman.store') }}">
        @csrf
        <div class="form-group">
            <label for="formGroupExampleInput">Akun</label>
            <select name="akun_id" id="" class="form-control">
                <option disabled hidden selected>Pilih Akun</option>
                @foreach ($akuns as $akun)
                    <option value="{{ $akun->akunId }}">{{ $akun->nama_akun }} - ( {{ $akun->kapasitas }} participant
                        )</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Event</label>
            <input type="text" class="form-control" name="event" placeholder="Masukan Event">
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Tanggal</label>
            <input type="date" class="form-control" name="tanggal" placeholder="Masukan Tanggal Peminjaman">
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Jam</label>
            <input type="time" class="form-control" name="jam" placeholder="Masukan Jam Peminjaman">
        </div>

        <div class="form-group">
            <label for="formGroupExampleInput">Durasi</label>
            <input type="text" class="form-control" name="durasi" placeholder="Masukan Durasi Peminjaman">
        </div>
        <button class="btn btn-primary">Submit</button>
    </form>
@endsection
