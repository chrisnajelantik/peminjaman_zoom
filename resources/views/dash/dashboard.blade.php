@extends('layouts/layout')

@section('content')
    <table id="example" class="table table-striped" style="width:100%">
        <thead>
            <tr>
                <th>Nama Akun</th>
                <th>Jumlah Participant Akun</th>
                <th>Kategori</th>
                <th>Status Akun</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($akuns as $akun)
                <tr>
                    <td>{{ $akun->nama_akun }}</td>
                    <td>{{ $akun->kapasitas }}</td>
                    <td>{{ $akun->kategori->nama_kategori }}</td>
                    <td>{{ $akun->status_akun }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>
@endsection
