<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = Hash::make('admin1234');
          // sie lomba
        User::create([
            'name' => 'admin1',
            'email' => 'admin@admin.com',
            'prodi' => '-',
            'password' => $password,
        ])->assignRole('admin');

    }
}
