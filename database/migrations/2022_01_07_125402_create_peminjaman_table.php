<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeminjamanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peminjaman', function (Blueprint $table) {
            $table->id();
            $table->text('event');
            $table->date('tanggal');
            $table->time('jam');
            $table->string('durasi');
            $table->enum("status", ["pending", "approved", "declined", "returned"])->default("pending");
            $table->text('keterangan')->nullable();
            $table->timestamps();

            $table->foreignId('user_id')->constrained('users');
            $table->foreignId('akun_id')->constrained('akun_zoom');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peminjaman');
    }
}
