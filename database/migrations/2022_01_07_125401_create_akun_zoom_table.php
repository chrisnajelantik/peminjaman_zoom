<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAkunZoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('akun_zoom', function (Blueprint $table) {
            $table->id();
            $table->string('nama_akun');
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('kapasitas');
            $table->enum("status_akun", ["aktif", "tidak aktif"])->default("aktif");
            $table->timestamps();

            $table->foreignId('kategori_id')->constrained('kategori');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('akun_zoom');
    }
}
