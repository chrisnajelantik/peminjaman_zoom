<?php

use App\Http\Controllers\AkunZoomController;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\PeminjamanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::group([
//     'middleware' => ['role:mahasiswa'],
//     'as' => 'mahasiswa.',
// ], function () {

//     Route::get('mahasiswa', function () {
//         return view('dash/dashboard');
//     })->name('dashboard');
// });

Route::group([
    'middleware' => ['auth'],
    'as' => 'dashboard.',
], function () {

    Route::get('/',[DashboardController::class, "index"])->name('dashboard');
    Route::resource('dashboard/peminjaman', PeminjamanController::class)->except(["update"]);


    Route::group([
        'middleware' => ['role:admin'],
    ], function () {
        Route::resource('dashboard/peminjaman', PeminjamanController::class)->only(["update"]);
        Route::resource('dashboard/kategori', KategoriController::class);
        Route::resource('dashboard/akun-zoom', AkunZoomController::class);
    });
});
