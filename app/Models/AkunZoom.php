<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AkunZoom extends Model
{
    use HasFactory;
    protected $table = "akun_zoom";
    protected $guarded = ["id"];

    public function peminjaman() { 
        return $this->hasOne(Peminjaman::class, "akun_id"); 
  }

  public function kategori() { 
    return $this->belongsTo(Kategori::class); 
}

}
