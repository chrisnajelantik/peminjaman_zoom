<?php

namespace App\Http\Controllers;

use App\Models\AkunZoom;
use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class AkunZoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $akuns = AkunZoom::all();
        return view('dash.akun_zoom.index', compact('akuns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Kategori::all();
        return view('dash.akun_zoom.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data["password"] = Crypt::encryptString($request->password);

        $kategori = Kategori::where("id", $request->kategori_id)->first();

        $kategori->update([
            "jumlah_akun" => $kategori->jumlah_akun +1
        ]);
        AkunZoom::create($data);
        return redirect()->route("dashboard.akun-zoom.index")->with('toast_success', "Akun Berhasil Dibuat");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AkunZoom  $akunZoom
     * @return \Illuminate\Http\Response
     */
    public function show(AkunZoom $akunZoom)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AkunZoom  $akunZoom
     * @return \Illuminate\Http\Response
     */
    public function edit(AkunZoom $akunZoom)
    {
        $categories = Kategori::all();
        return view('dash.akun_zoom.edit', compact('akunZoom', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AkunZoom  $akunZoom
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AkunZoom $akunZoom)
    {
        $data = $request->all();
        $data["password"] = Crypt::encryptString($request->password);

        $akunZoom->update($data);
        return redirect()->route("dashboard.akun-zoom.index")->with('toast_success', "Akun Berhasil Diupdate");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AkunZoom  $akunZoom
     * @return \Illuminate\Http\Response
     */
    public function destroy(AkunZoom $akunZoom)
    {
        $akunZoom->delete();
        return redirect()->route("dashboard.akun-zoom.index")->with('toast_warning', "Akun Telah Dihapus!");
    }
}
