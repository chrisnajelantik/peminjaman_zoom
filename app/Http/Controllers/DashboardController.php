<?php

namespace App\Http\Controllers;

use App\Models\AkunZoom;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
   public function index()
   {
       $akuns = AkunZoom::all();
    return view('dash.dashboard', compact('akuns'));
   }
}
