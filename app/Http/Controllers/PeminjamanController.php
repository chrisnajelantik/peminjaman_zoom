<?php

namespace App\Http\Controllers;

use App\Models\AkunZoom;
use App\Models\Kategori;
use App\Models\Peminjaman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class PeminjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasRole('mahasiswa')) {
            $peminjamans = Peminjaman::where('user_id', Auth::user()->id)->get();
        } else {
            $peminjamans = Peminjaman::all();
        }
        return view('dash.peminjaman.index', compact('peminjamans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $akuns = AkunZoom::leftJoin('peminjaman', function ($join) {
            $join->on('akun_zoom.id', '=', 'peminjaman.akun_id');
        })->whereNull('peminjaman.akun_id')
            ->orWhere("peminjaman.status", "!=", "approved")
            ->Where("peminjaman.status", "!=", "pending")
            ->where("akun_zoom.status_akun", "aktif")
            ->select("akun_zoom.*", "akun_zoom.id as akunId", "peminjaman.*")->get();

        return view('dash.peminjaman.create', compact('akuns'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $data["user_id"] = Auth::user()->id;
        Peminjaman::create($data);

        return redirect()->route("dashboard.peminjaman.index")->with('toast_success', "Peminjaman Berhasil Dibuat");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Peminjaman  $peminjaman
     * @return \Illuminate\Http\Response
     */
    public function show(Peminjaman $peminjaman)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Peminjaman  $peminjaman
     * @return \Illuminate\Http\Response
     */
    public function edit(Peminjaman $peminjaman)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Peminjaman  $peminjaman
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Peminjaman $peminjaman)
    {
        $kategori = Kategori::where("id", $peminjaman->akun->kategori_id)->first();
        $peminjaman->update([
            "status" => $request->status,
            "keterangan" => $request->status == "approved" ?  "email : " . $peminjaman->akun->email . "\r\npassword : " . Crypt::decryptString($peminjaman->akun->password) : $request->keterangan,
        ]);
        if ($request->status == "approved") {
            $kategori->update([
                "jumlah_akun" => $kategori->jumlah_akun - 1
            ]);
        } else {
            $kategori->update([
                "jumlah_akun" => $kategori->jumlah_akun + 1
            ]);
        }


        return redirect()->route("dashboard.peminjaman.index")->with('toast_success', "Peminjaman Berhasil Di Update");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Peminjaman  $peminjaman
     * @return \Illuminate\Http\Response
     */
    public function destroy(Peminjaman $peminjaman)
    {
        $peminjaman->delete();
        return redirect()->route("dashboard.peminjaman.index")->with('toast_warning', "Peminjaman Dibatalkan!");
    }
}
